import React, { useRef, useEffect, useState } from 'react';

export const Button = ({ className, children, onClick }) => (
  <button className={`rounded text-semibold ${className}`} onClick={onClick}>
    {children}
  </button>
);

export const Loader = () => {
    let circleCommonClasses = 'h-10 w-3 bg-current rounded-full';

    return (
        <div className='flex'>
            <div className={`${circleCommonClasses} mr-2 animate-bounce`}></div>
            <div
                className={`${circleCommonClasses} mr-2 animate-bounce200`}
            ></div>
            <div className={`${circleCommonClasses} animate-bounce400`}></div>
        </div>
    );
};

export const Modal = ({ isOpen=false, children }) => {
  const animationTiming = 175
  const modalRef = useRef(null);
  const animateTimerRef = useRef(null);
  const [ isModalVisible, setIsModalVisible ] = useState(isOpen);

  // setup modal animation
  useEffect(() => {
    clearTimeout(animateTimerRef.current);
    if (modalRef.current !== null) {
      if (isOpen) {
        modalRef.current.style = `animation: modal-open ${animationTiming}ms ease-in-out`
      } else {
        modalRef.current.style = `animation: modal-close ${animationTiming}ms ease-in-out`
        animateTimerRef.current = setTimeout(() => setIsModalVisible(false), animationTiming);
      }
    }

    return () => {
      clearTimeout(animateTimerRef.current);
    }

  }, [isOpen, setIsModalVisible, modalRef, isModalVisible]);

  // setup modal visibility
  useEffect(() => {
    if (isOpen) {
      setIsModalVisible(true);
    }
  }, [isOpen, setIsModalVisible]);


  const modal = (
    <div
      className="z-50 fixed h-full w-full top-0
        bg-black/60 flex justify-center items-center backdrop-blur"
        ref={modalRef}
        style={{opacity: 0}}
      >

      <div className="bg-gray-100 rounded-md flex flex-col items-center w-[500px]">
        {children}
      </div>
    </div>
  )

  return (
    <>
      {isModalVisible && modal}
    </>
  )
}
