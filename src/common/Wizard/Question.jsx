import React from 'react';


export const Question = ({ title, value, keyboard, actionButtons }) => (
    <>
      <h1 className="mb-5 text-4xl font-bold">{title}</h1>
      <input className="rounded-2xl text-5xl p-5 text-black drop-shadow-xl text-center w-4/6 focus:border-none" type="text" readOnly value={value} />
    </>
);
Question.defaultProps = { keyboard: true, actionButtons: true }