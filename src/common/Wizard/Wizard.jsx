import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { IoArrowBack, IoArrowForward } from 'react-icons/io5';

import { Button } from '../ui';
import { Keyboard } from './Keyboard';


export const Wizard = ({ children, onComplete=() => {}, onChange=() => {} }) => {
  const [step, setStep] = useState(0);
  const [value, setValue] = useState({});
  const [prevEnabled, setPrevEnabled] = useState(false);
  const [nextEnabled, setNextEnabled] = useState(false);
  const [keyboardEnabled, setKeyboardEnabled] = useState(false);
  const navigate = useNavigate();

  // Setup For Step
  let stepEl = children[step]
  let totalSteps = children.length;
  if (stepEl == null ) {
    stepEl = children
  }

  if (totalSteps == null) {
    totalSteps = 1
  }


  let stepKey
  if (step < totalSteps) {
    stepKey = stepEl.props.keyName
  }

  useEffect(() => {
    if ((step < totalSteps) && (stepEl != null)) {
      setPrevEnabled(true);
      setNextEnabled(stepEl.props.actionButtons);
      setKeyboardEnabled(stepEl.props.keyboard);
    } else {
      setPrevEnabled(false);
      setNextEnabled(false);
      setKeyboardEnabled(false);

      // TODO: Something better than this
      if (step === totalSteps) {
        onComplete(value);
        setStep(step + 1);
      }

    }
  },
  [step, totalSteps, stepEl, onComplete, value],
  );

  const changeStep = (change) => {
    const nextStep = step + change

    if (nextStep < 0) {
      navigate("/");
    }

    setStep(nextStep);
  }

  const getValue = (stepKey) => {
    return value[stepKey]
  }

  const updateValue = (stepKey, val) => {
    const updatedValue = {
      ...value,
      [stepKey]: val
    }
    setValue(updatedValue)
    onChange(updatedValue)
  }

  // TODO: Move into Keyboard Comp
  const keyboardOnClick = (key) => {
    const stepKey = children[step].props.keyName

    if (key === "back") {
      updateValue(stepKey, getValue(stepKey).slice(0, -1))
      return
    }

    if (key === "space") { key = " "; };
    updateValue(stepKey, getValue(stepKey) + key)
  }


  const displayStep = () => {
    if (step < totalSteps) {
      const props = { value: getValue(stepKey), actions: { updateValue, changeStep } }

      if (getValue(stepKey) == null) {
        updateValue(stepKey, "")
      }

      if (React.isValidElement(stepEl)) {
        return React.cloneElement(stepEl, props)
      }
    } else if (step === totalSteps) {

      return (
        <h1>Loading</h1>
      )

    } else{

      navigate("/completed")
      /*
      //TODO: Cancel on unmount
      setTimeout(() => {
        navigate("/")
      }, 2500);

      return (
        <>
          <h1 className="text-9xl font-welcome mt-5">Thank you...</h1>
          <p className="text-2xl p-8">Enjoy your day! :)</p>
        </>
      )*/
    }
  }



  return (
    <div className="grow flex items-stretch h-full pt-10">

      <div className="flex-shrink-0 w-96 h-64 self-center -ml-72">
        {
          prevEnabled &&
            <Button className="rounded-[125px] text-gray-100 bg-rose-500 hover:bg-rose-600 w-full h-full" onClick={() => ( changeStep(-1) )}>
              <div className="ml-72 flex place-content-center">
                <IoArrowBack size={60} />
              </div>
            </Button>
        }
      </div>

      <div className="grow flex flex-col items-center overflow-hidden">
        {
          displayStep(children[step])
        }
        {
          keyboardEnabled && <Keyboard onClick={keyboardOnClick} />
        }
      </div>
      <div className="flex-shrink-0 w-96 h-64 self-center -mr-72">
        {
          nextEnabled &&
            <Button className="rounded-[125px] text-gray-100 bg-primary h-full w-full" onClick={() => ( changeStep(1) )}>
              <div className="mr-72 flex place-content-center">
                <IoArrowForward size={60} />
              </div>
            </Button>
        }
      </div>

    </div>
  )
}