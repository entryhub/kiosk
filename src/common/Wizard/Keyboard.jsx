import React from 'react';

import { BiSpaceBar } from 'react-icons/bi';
import { IoBackspaceSharp } from 'react-icons/io5';
import { BsCapslock } from 'react-icons/bs';

import { Button } from '../ui';


export const Keyboard = ({ onClick }) => {
  const keyboard = [
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0","-"],
    ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"],
    ["a", "s", "d", "f", "g", "h", "j", "k", "l", "back"],
    [/*"caps",*/ "z", "x", "c", "v", "b", "n", "m", ",", "."],
    ["space"]
  ]

  //const keySound = new Audio("https://freesfx.co.uk/sound/16951_1461335341.mp3");

  const keyPress = (key) => {
    //keySound.play();
    onClick(key);
  };

  const Key = (props) => {
    let value = props.value;
    let width = "w-24";

    if (value === "space") { value = (<BiSpaceBar />); width = "w-96"; }
    else if (value === "back") { value = (<IoBackspaceSharp size={23}/>); width = "w-40"; }
    else if (value === "caps") { value = (<BsCapslock size={23}/>); width = "w-40"; }

    return (
      <Button
        className={`bg-slate-600 text-gray-100 hover:bg-slate-700 h-20 m-1 ${ width }`}
        onClick={props.onClick}
      >
        <div className="flex justify-center font-semibold text-3xl">
          {value}
        </div>
      </Button>
    );
  };

  return (
    <div className="flex flex-col items-center bg-slate-800 rounded-3xl p-3 mt-5" >
      {
        keyboard.map((row, index) => (
          <div key={index}>

            {
              row.map(key => ( <Key value={key} key={key} onClick={(el) => { keyPress(key) }} /> ))
            }

          </div>
        ))
      }
    </div>
  )
}

