import React from 'react';

import { Button } from '../ui';


const SelectionButton = ({ onClick, children, className }) => (
  <Button
    className={`bg-primary w-52 h-24 m-1 ${className}`}
    onClick={onClick}
  >
    <div className="flex flex-col place-content-center text-white">
      {children}
    </div>
  </Button>
)

export const Selection = ({ actions, keyName, title, children, className, currentPage, totalPages, load, next, prev, keyboard, actionButtons }) => {

  const childrenWithProps = React.Children.map(children, child => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, { onClick: () => { actions.updateValue(keyName, child.props.value); actions.changeStep(1) } });
    }
    return child;
  });

  return (
    <>
      <h1 className="mb-5 text-4xl font-bold">{title}</h1>
      <div className={`w-[68rem] h-[33rem] bg-white p-1 rounded-xl text-center className}`}>

        {childrenWithProps}

      </div>
    </>
  )
}

Selection.defaultProps = { currentPage: 1, totalPages: 1, keyboard: false, actionButtons: false }
Selection.Button = SelectionButton