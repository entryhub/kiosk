import React, { useEffect } from "react";
import { useNavigate } from 'react-router-dom';

export const Completed = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const timeout = setTimeout(() => {
        navigate("/")
      }, 2500);

    return () => clearTimeout(timeout);
  })

  return (
    <div className="grow flex flex-col items-center overflow-hidden mt-20">
      <h1 className="text-9xl font-welcome">Thank you...</h1>
      <p className="text-2xl p-8">Enjoy your day! :)</p>
    </div>
  )
}