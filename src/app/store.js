import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';

import { entrypointApi } from '../services';

export const store = configureStore({
  reducer: {
    [entrypointApi.reducerPath]: entrypointApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(entrypointApi.middleware),
});

setupListeners(store.dispatch);
