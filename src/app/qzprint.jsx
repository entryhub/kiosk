/* eslint-disable no-undef */
import React, { useEffect } from "react";
import { v4 as uuidv4 } from 'uuid';
import { Loader } from "../common/ui";

import * as qz from "qz-tray"

export const QzPrint = ({ children }) => {

  const [connected, setConnected] = React.useState(false);

  // TODO: Add button to skip connecting to qz-tray

  useEffect(() => {
    qz.security.setCertificatePromise(function(resolve, reject) {
      fetch(`/qz-certificate`, {cache: 'no-store', headers: {'Content-Type': 'text/plain'}})
        .then(function(data) { data.ok ? resolve(data.text()) : reject(data.text()); });
    });

    qz.security.setSignatureAlgorithm("SHA512"); // Since 2.1
    qz.security.setSignaturePromise((message) => {
      return (resolve, reject) => {
        fetch("/api/v1", {
          headers: { 'Content-Type': 'application/json' },
          cache: 'no-store',
          method: 'POST',
          body: JSON.stringify({
            jsonrpc: "2.0",
            method: "system.qz_sign",
            id: uuidv4(),
            params: {
              message
            }
          })
        })
          .then(response => { if (response.ok) { return response.json() } else { reject(response.text()) } })
          .then(data => resolve(data.result))
      };
    });
  });

  const connect = () => {
    return new Promise((resolve, reject) => {
      if (qz.websocket.isActive()) {
        resolve();
      } else {
        // try to connect once before firing the mimetype launcher
        qz.websocket.connect().then(resolve, function retry() {
          // if a connect was not successful, launch the mimetime, try 3 more times
          window.location.assign("qz:launch");
          qz.websocket.connect({ retries: 2, delay: 1 }).then(resolve, reject);
        });
      }
    });
  }

  const checkConnection = () => {
    setTimeout(() => {
      if(qz.websocket.isActive()) {
        checkConnection()
      } else {
        setConnected(false)

      }
    }, 1000);
  }

  useEffect(() => {
    if (!connected) {
      connect().then(() => {
        setConnected(true)
        checkConnection()
      }).catch((e) => {console.error(e)})
    }
  })

  if(connected) {
    return (
      <>
        {children}
      </>
    )
  } else {
    return (
      <div className="bg-slate-600 rounded text-gray-300 w-2/6 h-80 flex flex-col justify-center items-center mx-auto mt-10">
        <Loader />
        <h1 className="text-6xl font-welcome">Conncting to printer</h1>
      </div>
    )
  }
}