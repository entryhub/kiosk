/* eslint-disable no-undef */
import React from 'react';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { IoArrowBack } from 'react-icons/io5';
import { useLocalStorage } from '../../app/uselocalstorage';
import { Button } from '../../common/ui';

import * as qz from "qz-tray"

export const Setting = () => {

  const navigate = useNavigate();
  const [printers, setPrinters] = useState([]);
  const [badgePrinter, setBadgePrinter] = useLocalStorage('badgePrinter', null);
  const [firePrinter, setFirePrinter] = useLocalStorage('firePrinters', []);

  useEffect(() => {
    if (printers.length === 0) {
      qz.printers.find().then(printers => {
        setPrinters(printers)
      }).catch(function(e) { console.error(e); });
    }
  })

  const SelectionBox = ({ children, title }) => (
    <div className="w-4/12 bg-white rounded-lg flex flex-col m-5">
      <div className="bg-slate-600 rounded-t-lg h-14 text-gray-200 flex items-center justify-center font-semibold text-2xl shrink-0">
        <h1>{title}</h1>
      </div>
      <div className="grow overflow-hidden overflow-y-scroll m-1 px-2 ">
        {children}
      </div>
    </div>
  )

  return (
    <div className="flex items-stretch h-full pt-10 w-full">

      <div className="flex-shrink-0 w-96 h-64 self-center -ml-72">
            <Button className="rounded-[125px] text-gray-100 bg-rose-500 hover:bg-rose-600 w-full h-full" onClick={() => ( navigate("/") )}>
              <div className="ml-72 flex place-content-center">
                <IoArrowBack size={60} />
              </div>
            </Button>

      </div>

      <div className="flex overflow-hidden w-full h-full justify-center self-stretch">

        <SelectionBox title="Badge Printer">
          {printers.map((printer, index) => (
            <Button key={index} onClick={() => setBadgePrinter(printer)} className={`h-24 mb-1 w-full ${badgePrinter === printer ? "bg-green-500" : "bg-primary"}`}>{printer}</Button>
          ))}
        </SelectionBox>

        <SelectionBox title="Fire Printer">
          {printers.map((printer, index) => {
            const handleClick = () => {
              if (firePrinter.includes(printer)) {
                setFirePrinter(firePrinter.filter(item => item !== printer))
              } else {
                setFirePrinter([ ...firePrinter, printer ])
              }
            }
            return (
              <Button key={index} onClick={handleClick} className={`h-24 mb-1 w-full ${firePrinter.includes(printer) ? "bg-green-500" : "bg-primary"}`}>{printer}</Button>
            )
            })}
        </SelectionBox>

      </div>
      <div className="flex-shrink-0 w-96 h-64 self-center -mr-72">
      </div>

    </div>
  )
}