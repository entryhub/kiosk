import React from 'react';

import { Wizard, Question } from '../../common/Wizard'
import { useCreateVisitorMutation } from '../../services';
import { useLocalStorage } from '../../app/uselocalstorage';

import * as qz from "qz-tray"

import inSfx from '../../sounds/signin.wav';

const inSound = new Audio(inSfx);


export const VisitorSignIn = () => {
  const [ createVisitor ] = useCreateVisitorMutation();
  const [ badgePrinter ] = useLocalStorage('badgePrinter', null);

  const printBadge = ({id, badgePdf}) => {
    if (badgePrinter !== null) {
      // eslint-disable-next-line no-undef
      let config = qz.configs.create(badgePrinter, { jobName: `Visitor Badge ${id}`, scaleContent: false, margins: 5, units: "mm" });
      let data = [{
        type: 'Pixel',
        format: 'pdf',
        flavor: 'base64',
        data: `${badgePdf}`
      }];

      // eslint-disable-next-line no-undef
      qz.print(config, data).catch((e) => console.error(e));
    }
  }

  return (
    <Wizard onComplete={(value) => { createVisitor({ ...value }).then( ({ data }) => { inSound.play(); printBadge(data) })}}>
      <Question keyName="name" title="Please enter your name" />
      <Question keyName="company" title="Please enter your company" />
      <Question keyName="carRegistration" title="Please enter your car registration" />
      <Question keyName="reason" title="Please enter reason for visiting" />
    </Wizard>
  )
}