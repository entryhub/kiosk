import React from 'react';

import { Wizard, Selection } from '../../common/Wizard'
import { useGetVisitorsQuery, useSignOutVisitorMutation } from '../../services';

import outSfx from '../../sounds/signout.wav';

const outSound = new Audio(outSfx);

export const VisitorSignOut = () => {
  const { data, error, isLoading } = useGetVisitorsQuery({ status: "in" }, { refetchOnMountOrArgChange: true, pollingInterval: 3000 });
  const [ signOutVisitor ] = useSignOutVisitorMutation();

  // TODO: Handle errors better!
  if (error != null) {
    alert(`Error: ${error.data.error.code} ${error.data.error.message}`)
  }

  let visitors = (
    <h1>Loading</h1>
  )
  if ((!isLoading) && (data != null)) {
    visitors = data.map((v, index) => {
      return (
        <Selection.Button key={index} value={v.id}>
          <p className="text-l font-semibold h-5">{v.name}</p>
          <p className="text-xs text-slate-10 h-5">{v.company}</p>
        </Selection.Button>
      )
    });
  }

  return (
    <Wizard onComplete={(value) => { signOutVisitor({ id: value.visitor }).then(() => outSound.play()) }}>
      <Selection title="Please select your name" keyName="visitor">
        {visitors}
      </Selection>
    </Wizard>
  )
}