import React from 'react';

import { Wizard, Selection, Question } from '../../common/Wizard'
import { useCreateLatePupilMutation } from '../../services';

import inSfx from '../../sounds/signin.wav';

const inSound = new Audio(inSfx);


export const LatePupil = () => {
  const [ createLatePupil ] = useCreateLatePupilMutation();


  const settingsClassNames = [
    "Nursery",
    "Reception",
    "Year 1",
    "Year 1/2",
    "Year 2",
    "Year 3",
    "Year 3/4",
    "Year 4",
    "Year 5",
    "Year 5/6",
    "Year 6"
  ]

   const classNameSelection = settingsClassNames.map((className, index) => {
      return (
        <Selection.Button key={index} value={className}>
          <p className="text-xl font-semibold ">{className}</p>
        </Selection.Button>
      )
    });



  return (
    <Wizard onComplete={(value) => { createLatePupil({ ...value }).then( ()=> inSound.play() ) }}>
      <Selection keyName="className" title="Please select your child's class">
        {/* TODO: Get class names from settings */}
        {classNameSelection}
      </Selection>
      <Question keyName="name" title="Please enter your child's Name" />
      <Question keyName="reason" title="Please enter the reason for lateness" />
    </Wizard>
  )
}