import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { FaChild, FaCogs, FaFireAlt } from 'react-icons/fa';
import { BsPersonBadgeFill, BsPeopleFill, BsPeople } from 'react-icons/bs';

import { useGetFireListMutation } from '../../services';
import { useSignInOutStaffMutation, useSignOutVisitorMutation } from '../../services';
import { useLocalStorage } from '../../app/uselocalstorage';

import inSfx from '../../sounds/signin.wav';
import outSfx from '../../sounds/signout.wav';

import * as qz from "qz-tray"

const inSound = new Audio(inSfx);
const outSound = new Audio(outSfx);


const ButtonLink = ({ children, to }) => (
  <Link className="rounded-2xl text-gray-100 bg-primary h-60 text-4xl flex flex-col justify-center items-center" to={to}>{children}</Link>
);

export const Menu = () => {

  const [firePrinter] = useLocalStorage('firePrinters', []);
  const [getFireList] = useGetFireListMutation()
  const [barcode, setBarcode] = React.useState('');
  const [visitorSignOut] = useSignOutVisitorMutation();
  const [staffSignInOut] = useSignInOutStaffMutation();
  const navigate = useNavigate();

  useEffect(() => {
  const BarcodeReadEvent = (ev) => {
    if (ev.keyCode === 13) {
      if (barcode.charAt(0).toUpperCase() === "V") {
        visitorSignOut({ badgeToken: barcode }).then(() => {
          outSound.play();
          navigate("/completed");
        });
      } else {
        staffSignInOut({ token: barcode }).then(({data: {status}}) => {
          status === "in" ? inSound.play() : outSound.play()
          navigate("/completed");
        })
      }

      console.log("barcode", barcode)
      setBarcode("")
    } else if (ev.key.length === 1) {
      setBarcode(barcode + ev.key)
    }
  }

  document.addEventListener("keydown", BarcodeReadEvent)

  return () => {
    // Passing the same reference
    document.removeEventListener("keydown", BarcodeReadEvent)
  }
})

  // TODO: Put some where better and centralize
  const printFireList = () => {
    if (firePrinter !== []) {
      getFireList().then(({data: {report}}) => {
        console.log(report)
        firePrinter.forEach(printer => {
          // eslint-disable-next-line no-undef
          let config = qz.configs.create(printer, { jobName: `Fire List`, scaleContent: true, margins: 5, units: "mm" });
          let data = [{
            type: 'Pixel',
            format: 'pdf',
            flavor: 'base64',
            data: `${report}`
          }];

          // eslint-disable-next-line no-undef
          qz.print(config, data).catch((e) => console.error(e));
        })
      })
    }
  }

  return (
    <div className="flex flex-col h-full grow justify-between pt-20">
      <div className="flex justify-items-center justify-center items-center">
        <div className="flex-none w-6/12 flex flex-col items-center self-start">
          <h1 className="text-9xl font-welcome">Welcome...</h1>
          <p className="text-xl">Select an option or scan you barcode to begin</p>
        </div>
        <div className="flex-1 grid grid-cols-2 gap-4 pr-5">
          <ButtonLink to="/visitor/signin"><span className="p-3">Visitor In</span><BsPeopleFill size={100} /></ButtonLink>
          <ButtonLink to="/visitor/signout"><span className="p-3">Visitor Out</span><BsPeople size={100} /></ButtonLink>
          <ButtonLink to="/pupil/signin"><span className="p-3">Late Pupil</span><FaChild size={100} /></ButtonLink>
          <ButtonLink to="/staff"><span className="p-3">Staff</span><BsPersonBadgeFill size={100} /></ButtonLink>
        </div>
      </div>
      <div className="flex justify-end text-gray-600">
        <button onClick={ () => printFireList() } className="p-3 inline-block rounded bg-primary mr-2"><FaFireAlt size={60}/></button>
        <Link className="p-3 inline-block rounded bg-primary mr-2" to="/settings"><FaCogs size={60}/></Link>
      </div>
    </div>
  );
};