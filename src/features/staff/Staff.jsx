
import React from 'react';
import { useState } from 'react';

import { Wizard, Selection } from '../../common/Wizard'
import { useGetStaffQuery, useSignInOutStaffMutation } from '../../services';

import inSfx from '../../sounds/signin.wav';
import outSfx from '../../sounds/signout.wav';

const inSound = new Audio(inSfx);
const outSound = new Audio(outSfx);

export const Staff = () => {

  const [ dayOfBirth, setDayOfBirth ] = useState(0);
  const { data=[], error, isLoading, isFetching } = useGetStaffQuery({ dayOfBirth }, { refetchOnMountOrArgChange: true });
  const [ signInOutStaff ] = useSignInOutStaffMutation();


  let dateButtons = [];
    for (let i = 1; i < 32; i++) {
      dateButtons.push(
        <Selection.Button value={i} className="w-[9.1rem]">
          <p className="text-xl font-semibold ">{i}</p>
        </Selection.Button>
      );
    }

  // TODO: Handle errors better!
  if (error != null) {
    alert(`Error: ${error.data.error.code} ${error.data.error.message}`)
  }

  let staff

  if ((!isLoading) && (!isFetching)) {
    staff = data.map((s) => {
      let firstName = ""
      if (s.firstName != null) {
        firstName = s.firstName.charAt(0)
      }

      return (
        <Selection.Button value={s.id}>
          <p className="text-xl font-semibold h-5">{firstName} {s.lastName}</p>
          <p className="text-lg text-slate-10 h-5">{s.status}</p>
        </Selection.Button>
      )
    });
  }


  return (
    <Wizard
      onChange={ (val) => { setDayOfBirth(val.dob === "" ? 0 : val.dob); } }
      onComplete={ (val) => {signInOutStaff({ id: val.staffId }).then(({data: {status}}) => { status === "in" ? inSound.play() : outSound.play() })}}
    >
      <Selection keyName="dob" title="Please select your date of your birth">
        {dateButtons}
      </Selection>

      <Selection keyName="staffId" title="Please select your name">
        {staff}
      </Selection>
    </Wizard>
  )
}