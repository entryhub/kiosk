import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { v4 as uuidv4 } from 'uuid';


// Define a service using a base URL and expected endpoints
export const entrypointApi = createApi({
  baseQuery: fetchBaseQuery({ baseUrl: '/api/v1' }),
  tagTypes: ['Visitor', 'Staff', 'StaffLog', 'LatePupil'],
  endpoints: (build) => ({

    getSystemHealth: build.query({
      query: () => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "system.health",
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
    }),

    getVisitors: build.query({
      query: ({status="all", dateTo=null, dateFrom=null, search=null}) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "visitors.list",
          "params": {
            status,
            dateTo,
            dateFrom,
            search
          },
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Visitor', id })),
              { type: 'Visitor', id: 'LIST' },
            ]
          : [{ type: 'Visitor', id: 'LIST' }],
    }),

    signOutVisitor: build.mutation({
      query: ({ id=null, badgeToken=null  }) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "visitors.sign_out",
          "params": {
            id,
            badgeToken
          },
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
      invalidatesTags: (result, error, arg) => [{ type: 'Visitor', id: arg }]
    }),

    createVisitor: build.mutation({
      query: ({ name, company, reason, carRegistration }) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "visitors.create",
          "params": {
            name,
            company,
            reason,
            carRegistration
          },
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
      invalidatesTags: (result, error, arg) => [{ type: 'Visitor', id: 'LIST' }]
    }),

    getStaff: build.query({
      query: ({ dayOfBirth=0, search=null }) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "staff.list",
          "params": {
            dayOfBirth,
            search
          },
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Staff', id })),
              { type: 'Staff', id: 'LIST' },
            ]
          : [{ type: 'Staff', id: 'LIST' }],
    }),

    getStaffDetails: build.query({
      query: (id) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "staff.details",
          "params": {
            "id": id
          },
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
      providesTags: (result, error, arg) => [{ type: 'Staff', id: arg }]
    }),

    getStaffLogs: build.query({
      query: ({ id, dateFrom, dateTo }) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "staff.logs",
          "params": {
            id,
            dateFrom,
            dateTo
          },
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
      providesTags: (result, error, arg) => [{ type: 'StaffLog', id: arg.id }]
    }),

    signInOutStaff: build.mutation({
      query: ({ id=null, token=null }) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "staff.sign_in_out",
          "params": {
            id,
            token
          },
          "id": uuidv4()
        },
      }),
      transformResponse: (response, meta, arg) => response.result,
      invalidatesTags: (result, error, arg) => [{ type: 'Staff', id: arg }, { type: 'StaffLog', id: arg }]
    }),

    createLatePupil: build.mutation({
      query: ({ name, className, reason }) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "latepupils.create",
          "params": {
            name,
            className,
            reason
          },
          "id": uuidv4()
        }
      }),
      invalidatesTags: (result, error, arg) => [{ type: 'LatePupil', id: 'LIST' }]
    }),

    getLatePupils: build.query({
      query: ({ dateTo=null, dateFrom=null, search=null }) => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "latepupils.list",
          "params": {
            dateTo,
            dateFrom,
            search
          },
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'LatePupil', id })),
              { type: 'LatePupil', id: 'LIST' },
            ]
          : [{ type: 'LatePupil', id: 'LIST' }],
    }),

    getWhosIn: build.query({
      query: () => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "system.whosin",
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
    }),

    getFireList: build.mutation({
      query: () => ({
        method: 'POST',
        body: {
          "jsonrpc": "2.0",
          "method": "system.firelist",
          "id": uuidv4()
        }
      }),
      transformResponse: (response, meta, arg) => response.result,
    }),
  })
})

export const {
  useGetSystemHealthQuery,
  useGetVisitorsQuery,
  useSignOutVisitorMutation,
  useCreateVisitorMutation,
  useGetStaffQuery,
  useGetStaffDetailsQuery,
  useGetStaffLogsQuery,
  useSignInOutStaffMutation,
  useCreateLatePupilMutation,
  useGetLatePupilsQuery,
  useGetWhosInQuery,
  useGetFireListMutation,
} = entrypointApi