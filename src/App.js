import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Moment from 'react-moment';

import { Menu } from './features/menu/Menu';
import { VisitorSignIn } from './features/visitors/visitorsignin';
import { VisitorSignOut } from './features/visitors/visitorsignout';
import { LatePupil } from './features/latePupil/latepupil';
import { Staff } from './features/staff/Staff';

import './App.css';
import background from "./jess-bailey-l3N9Q27zULw-unsplash.jpg";
import { QzPrint } from './app/qzprint';
import { ScreenIdle } from './app/screenidle';
import { Setting } from './features/setting/setting';

import { Completed } from './app/completed';
import { SystemHealth } from './app/systemhealth';


function App() {

  return (
    <div style={{ backgroundImage: `url(${background})` }} className="flex flex-col h-screen w-screen font-body text-gray-800 bg-bottom bg-cover">
      <header className="flex-initial flex justify-center text-gray-100 bg-slate-600 shrink-0">
        {/*<img src={logo} alt="Logo" className=" h-44" />*/}
        <div className="flex p-5 items-center justify-between text-2xl grow">
          {/* TODO: Move away from moment */}
          <Moment interval={30000} format="dddd, Do MMM" />
          <Moment interval={10000} format="h:mm a" />
        </div>
      </header>
      <main className="flex grow overflow-hidden w-full h-full">
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <SystemHealth>
            <QzPrint>
              <ScreenIdle>
                <Routes>
                  <Route exact path="/" element={<Menu />} />
                  <Route exact path="/completed" element={<Completed />} />
                  <Route exact path="/visitor/signin" element={<VisitorSignIn />} />
                  <Route exact path="/visitor/signout" element={<VisitorSignOut />} />
                  <Route exact path="/pupil/signin" element={<LatePupil />} />
                  <Route exact path="/staff" element={<Staff />} />
                  <Route exact path="/settings" element={<Setting />} />
                </Routes>
              </ScreenIdle>
            </QzPrint>
          </SystemHealth>
        </BrowserRouter>
      </main>

      <footer className="p-3 text-sm shrink-0 flex justify-between">
        <p>Background by Jess Bailey - unsplash.com</p>
        <p>Powered by Emmet</p>
      </footer>
    </div>
  );
}

export default App;
