module.exports = {
  mode: 'jit',
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#2a7ca8',
      },
      fontFamily: {
        'welcome': ['"Neonderthaw"', 'cursive'],
        'body': ['"Ubuntu"'],
      },
      animation: {
        bounce200: 'bounce 1s infinite 200ms',
        bounce400: 'bounce 1s infinite 400ms',
      },
    },
  },
  plugins: [
    require('tailwind-scrollbar'),
  ],
}